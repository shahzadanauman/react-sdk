import { useState } from 'react';
import { withKnobs } from '@storybook/addon-knobs';

import { configProps, stateProps } from './mock.stories';

import LcsNeomCustomDateTime from './index.tsx';

export default {
  title: 'LcsNeomCustomDateTime',
  decorators: [withKnobs],
  component: LcsNeomCustomDateTime
};

if (!window.PCore) {
  window.PCore = {};
}

window.PCore.getEnvironmentInfo = () => {
  return {
    getUseLocale: () => {
      return 'en-GB';
    }
  };
};

export const BaseLcsNeomCustomDateTime = () => {
  const [value, setValue] = useState(configProps.value);

  const props = {
    value,
    label: configProps.label,
    hideLabel: configProps.hideLabel,
    displayMode: configProps.displayMode,
    getPConnect: () => {
      return {
        getActionsApi: () => {
          return {
            updateFieldValue: (propName, theValue) => {
              setValue(theValue);
            },
            triggerFieldChange: () => {/* nothing */}
          };
        },
        getStateProps: () => {
          return stateProps;
        }
      };
    },
    onChange: dateTime => {
      setValue(dateTime.value);
    }
  };

  return (
    <>
      <LcsNeomCustomDateTime {...props} />
    </>
  );
};
