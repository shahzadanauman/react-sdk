/* eslint-disable react/no-unused-prop-types */
import React, { useEffect, useState } from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import handleEvent from '@pega/react-sdk-components/lib/components/helpers/event-utils';
import Utils from '@pega/react-sdk-components/lib/components/helpers/utils';
import PropTypes from 'prop-types';
import FieldValueList from '@pega/react-sdk-components/lib/components/designSystemExtension/FieldValueList';

import StyledLcsNeomChecklistTest1Wrapper from './styles';

interface IOption {
  key: string;
  value: string;
}

export default function LcsNeomChecklistTest1(props: any) {
  const {
    getPConnect,
    label,
    required,
    disabled,
    value = [],
    datasource = [],
    validatemessage,
    status,
    readOnly,
    testId,
    helperText,
    displayMode,
    hideLabel,
    onRecordChange,
    fieldMetadata
  } = props;

  const [options, setOptions] = useState<Array<IOption>>([]);
  const helperTextToDisplay = validatemessage || helperText;

  const thePConn = getPConnect();
  const actionsApi = thePConn.getActionsApi();
  const propName = thePConn.getStateProps().value;
  const className = thePConn.getCaseInfo().getClassName();
  const refName = propName?.slice(propName.lastIndexOf('.') + 1);

  useEffect(() => {
    const list = Utils.getOptionList(props, getPConnect().getDataObject());
    setOptions(list);
  }, [datasource]);

  let readOnlyProp = {};

  if (readOnly) {
    readOnlyProp = { readOnly: true };
  }

  let testProp = {};
  testProp = {
    'data-test-id': testId
  };

  const handleCheckboxChange = (optionKey: string) => {
    let updatedValues = [...value];

    if (value.includes(optionKey)) {
        updatedValues = updatedValues.filter(v => v !== optionKey);
    } else {
        updatedValues.push(optionKey);
    }

    handleEvent(actionsApi, 'changeNblur', propName, updatedValues);
    if (onRecordChange) {
        onRecordChange(updatedValues);
    }
  };

  return options.length === 0 ? null : (
    <StyledLcsNeomChecklistTest1Wrapper>
        {options.map((option: IOption) => (
            <FormControlLabel
                key={option.key}
                control={
                    <Checkbox
                        checked={value.includes(option.key)}
                        onChange={() => handleCheckboxChange(option.key)}
                        {...readOnlyProp}
                        {...testProp}
                    />
                }
                label={thePConn.getLocalizedValue(
                    option.value,
                    // remaining parameters of getLocalizedValue if required
                )}
            />
        ))}
    </StyledLcsNeomChecklistTest1Wrapper>
  );
}

LcsNeomChecklistTest1.defaultProps = {
  value: [],
  validatemessage: '',
  helperText: '',
  displayAsStatus: false,
  datasource: [],
  hideLabel: false,
  disabled: false,
  readOnly: false,
  required: false,
  testId: null,
  displayMode: null,
  variant: 'inline',
  formatter: ''
};

LcsNeomChecklistTest1.propTypes = {
  value: PropTypes.arrayOf(PropTypes.string),
  displayMode: PropTypes.string,
  displayAsStatus: PropTypes.bool,
  label: PropTypes.string.isRequired,
  hideLabel: PropTypes.bool,
  getPConnect: PropTypes.func.isRequired,
  validatemessage: PropTypes.string,
  helperText: PropTypes.string,
  datasource: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.any),
    PropTypes.shape({
      source: PropTypes.arrayOf(PropTypes.any)
    })
  ]),
  disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  readOnly: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  testId: PropTypes.string,
  variant: PropTypes.string,
  formatter: PropTypes.string
};
