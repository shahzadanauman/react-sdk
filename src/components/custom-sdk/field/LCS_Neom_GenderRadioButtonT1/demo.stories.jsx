import { useState } from 'react';
import { withKnobs } from '@storybook/addon-knobs';
import { configProps, stateProps } from './mock.stories';

import LcsNeomGenderRadioButtonT1 from './index.tsx';

export default {
  title: 'LcsNeomGenderRadioButtonT1',
  decorators: [withKnobs],
  component: LcsNeomGenderRadioButtonT1
};

export const BaseLcsNeomGenderRadioButtonT1 = () => {
  const [value, setValue] = useState(configProps.value);

  const props = {
    value,
    placeholder: configProps.placeholder,
    label: configProps.label,
    helperText: configProps.helperText,
    datasource: configProps.datasource,
    testId: configProps.testId,
    hasSuggestions: configProps.hasSuggestions,
    listType: configProps.listType,
    getPConnect: () => {
      return {
        getDataObject: () => {
          return undefined;
        },
        getStateProps: () => {
          return stateProps;
        },
        getActionsApi: () => {
          return {
            updateFieldValue: (propName, val) => {
              setValue(val);
            },
            triggerFieldChange: () => {
              return undefined;
            }
          };
        },
        getCaseInfo: () => {
          return {
            getClassName: () => {
              return undefined;
            }
          };
        },
        getLocalizedValue: val => {
          return val;
        },
        getLocaleRuleNameFromKeys: () => {
          return undefined;
        }
      };
    }
  };

  return (
    <>
      <LcsNeomGenderRadioButtonT1 {...props} />
    </>
  );
};

