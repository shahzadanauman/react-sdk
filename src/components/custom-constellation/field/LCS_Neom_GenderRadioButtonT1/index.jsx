/* eslint-disable react/no-unused-prop-types */
import React, { useEffect, useState } from 'react';

import { RadioGroup, FormControlLabel, Radio } from '@material-ui/core';

import handleEvent from '@pega/react-sdk-components/lib/components/helpers/event-utils';
import Utils from '@pega/react-sdk-components/lib/components/helpers/utils';
import PropTypes from 'prop-types';
import FieldValueList from '@pega/react-sdk-components/lib/components/designSystemExtension/FieldValueList';
import StyledLcsNeomGenderRadioButtonT1Wrapper from './styles';



export default function LcsNeomGenderRadioButtonT1(props) {
  const {
    getPConnect,
    label,
    required,
    disabled,
    value = '',
    datasource = [],
    validatemessage,
    status,
    readOnly,
    testId,
    helperText,
    displayMode,
    hideLabel,
    onRecordChange,
    fieldMetadata
  } = props;
  const [options, setOptions] = useState([]);

  const thePConn = getPConnect();
  const propName = thePConn.getStateProps().value;
  const className = thePConn.getCaseInfo().getClassName();

  useEffect(() => {
    const list = Utils.getOptionList(props, getPConnect().getDataObject());
    setOptions(list);
  }, [datasource]);

  const metaData = Array.isArray(fieldMetadata)
    ? fieldMetadata.find((field) => field?.classID === className)
    : fieldMetadata;

  if (displayMode === 'LABELS_LEFT' || displayMode === 'STACKED_LARGE_VAL') {
    return <FieldValueList name={hideLabel ? '' : label} value={value} variant={displayMode === 'STACKED_LARGE_VAL' ? 'stacked' : undefined} />;
  }

  const handleChange = evt => {
    const selectedValue = evt.target.value;
    handleEvent(thePConn.getActionsApi(), 'changeNblur', propName, selectedValue);
    if (onRecordChange) {
      onRecordChange(evt);
    }
  };

  return options.length === 0 ? null : (
    <StyledLcsNeomGenderRadioButtonT1Wrapper>
      <RadioGroup
        aria-label={label}
        name={label}
        value={value}
        onChange={!readOnly ? handleChange : undefined}
        data-test-id={testId}
      >
        {options.map((option) => (
          <FormControlLabel
            key={option.key}
            value={option.key}
            control={<Radio />}
            label={thePConn.getLocalizedValue(option.value)}
            disabled={disabled}
          />
        ))}
      </RadioGroup>
    </StyledLcsNeomGenderRadioButtonT1Wrapper>
  );
}

LcsNeomGenderRadioButtonT1.defaultProps = {
  value: '',
  validatemessage: '',
  helperText: '',
  displayAsStatus: false,
  datasource: [],
  hideLabel: false,
  disabled: false,
  readOnly: false,
  required: false,
  testId: null,
  displayMode: null,
  variant: 'inline',
  formatter: ''
};

LcsNeomGenderRadioButtonT1.propTypes = {
  value: PropTypes.string,
  displayMode: PropTypes.string,
  displayAsStatus: PropTypes.bool,
  label: PropTypes.string.isRequired,
  hideLabel: PropTypes.bool,
  getPConnect: PropTypes.func.isRequired,
  validatemessage: PropTypes.string,
  helperText: PropTypes.string,
  datasource: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.any),
    PropTypes.shape({
      source: PropTypes.arrayOf(PropTypes.any)
    })
  ]),
  disabled: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  readOnly: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  testId: PropTypes.string,
  variant: PropTypes.string,
  formatter: PropTypes.string
};
