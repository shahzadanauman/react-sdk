// Statically load all "local" components that aren't yet in the npm package

import LcsNeomCustomText from './src/components/custom-sdk/field/LCS_Neom_CustomText/';
import LcsNeomCustomDateTime from './src/components/custom-sdk/field/LCS_Neom_CustomDateTime/';
import LcsNeomCustomPickList from './src/components/custom-sdk/field/LCS_Neom_CustomPickList/';
/*import end - DO NOT REMOVE*/

// localSdkComponentMap is the JSON object where we'll store the components that are
// found locally. If not found here, we'll look in the Pega-provided component map

const localSdkComponentMap = {
  "LCS_Neom_CustomText" : LcsNeomCustomText,
  "LCS_Neom_CustomDateTime" : LcsNeomCustomDateTime,
  "LCS_Neom_CustomPickList" : LcsNeomCustomPickList,
/*map end - DO NOT REMOVE*/
};

export default localSdkComponentMap;
